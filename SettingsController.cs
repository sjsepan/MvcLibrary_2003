using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml.Serialization;
// using System.Text.Json;
// using System.Text.Json.Serialization;
using Ssepan.Utility;
using Ssepan.Io;
using Ssepan.Application;

namespace MvcLibrary
{
    /// <summary>
    /// Manager for the persisted Settings. 
    /// </summary>
    /// <typeparam name="MVCSettings"></typeparam>
    public  class SettingsController //:
//            ISettings
    {
        #region Declarations
        public const String FILE_NEW = "(new)";
        #endregion Declarations

        #region Constructors
        #endregion Constructors

        #region Properties
        private static MVCSettings _Settings = null;
        public static MVCSettings Settings//TODO:notify here as well?
        {
            get { return _Settings; }
            set 
            {
                if (DefaultHandler != null)
                {
                    if (_Settings != null)
                    {
                        _Settings.PropertyChanged -= DefaultHandler;
                    }
                }

                _Settings = value;

                if (DefaultHandler != null)
                {
                    if (_Settings != null)
                    {
                        _Settings.PropertyChanged += DefaultHandler;
                    }
                }
            }
        }

        private static String _OldPathname = null;
        /// <summary>
        /// Previous value of Pathname
        /// Set when filename changes.  Not synchronized here, but client apps may override and sychronize if it suits them.
        /// </summary>
        public static String OldPathname
        {
            get { return _OldPathname; }
            set
            {
                _OldPathname = value;
            }
        }

        private static String _OldFilename = null;
        /// <summary>
        /// Previous value of Filename.
        /// Set when filename changes.  Not synchronized here, but client apps may override and sychronize if it suits them.
        /// </summary>
        public static String OldFilename
        {
            get { return _OldFilename; }
            set
            {
                _OldFilename = value;
            }
        }

        private static String _Filename = FILE_NEW; 
        /// <summary>
        /// Filename component of FilePath
        /// </summary>
        public static String Filename
        {
            get { return _Filename; }
            set
            {
                if (StringExtensions.IsNullOrEmpty(value))
                {
                    //just clear property
                    OldFilename = _Filename;//remember previous name
                    _Filename = String.Empty;
                }
                else if (Path.GetDirectoryName(value) != String.Empty)
                {
                    //send to be split first
                    FilePath = value;
                }
                else
                {
                    //just set property to value
                    OldFilename = _Filename;//remember previous name
                    _Filename = value;
                }
            }
        }

        private static String _Pathname = PathExtensions.WithTrailingSeparator(Environment.GetFolderPath(Environment.SpecialFolder.Personal)); 
        /// <summary>
        /// Path component of FilePath
        /// </summary>
        public static String Pathname
        {
            get { return _Pathname; }
            set
            {
                if (StringExtensions.IsNullOrEmpty(value))
                {
                    //just clear property
                    OldPathname = _Pathname;
                    _Pathname = value;
                }
                else if (Path.GetFileName(value) != String.Empty)
                {
                    //send to be split first
                    FilePath = value;
                }
                else
                {
                    //just set property to value
                    OldPathname = _Pathname;
                    _Pathname = value;
                }
            }
        }

        /// <summary>
        /// Combined value of Pathname and Filename
        /// </summary>
        public static String FilePath
        {
            get 
            { 
                //retrieve and combine
                return Path.Combine(Pathname, Filename); 
            }
            set
            {
                //split and store
                Filename = Path.GetFileName(value);
                if (!Path.GetDirectoryName(value).EndsWith(Path.DirectorySeparatorChar.ToString()))
                {
                    //add separator or Pathname setter will think path still contains filename
                    Pathname = PathExtensions.WithTrailingSeparator(Path.GetDirectoryName(value));
                }
            }
        }
        
        private static PropertyChangedEventHandler _DefaultHandler = null;
        /// <summary>
        /// Handler to assigned to Settings; triggered on New, Open.
        /// </summary>
        public static PropertyChangedEventHandler DefaultHandler
        {
            get { return _DefaultHandler; }
            set 
            {
                if (DefaultHandler != null)
                {
                    if (Settings != null)
                    {
                        Settings.PropertyChanged -= DefaultHandler;
                    }
                }

                _DefaultHandler = value;

                if (DefaultHandler != null)
                {
                    if (Settings != null)
                    {
                        Settings.PropertyChanged += DefaultHandler;
                    }
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// New settings
        /// </summary>
        /// <returns></returns>
        public static Boolean New()
        {
            Boolean returnValue = false;
            try
            {//DEBUG:why is settings controller calling new before defaulthandler is set?
                //create new object
                Settings = new MVCSettings();//TODO:move this class into app and use specific types
                Settings.Sync();
                Filename = FILE_NEW;

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
            return returnValue;
        }

        /// <summary>
        /// Open settings.
        /// </summary>
        /// <returns></returns>
        public static Boolean Open()
        {
            Boolean returnValue = false;

            try
            {
                //read from file
                switch (MVCSettings.SerializeAs)
                //switch (Settings.SerializeAs)
                {
                    // case MVCSettings.SerializationFormat.Json:
                    //     {
                    //         Settings = SettingsController.LoadJson(FilePath);

                    //         break;
                    //     }
//                    case MVCSettings.SerializationFormat.DataContract:
//                        {
//                            Settings = SettingsController.LoadDataContract(FilePath);
//
//                            break;
//                        }
                    case MVCSettings.SerializationFormat.Xml:
                    default:
                        {
                            Settings = SettingsController.LoadXml(FilePath);

                            break;
                        }
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }

            return returnValue;
        }

        /// <summary>
        /// Save settings.
        /// </summary>
        /// <returns></returns>
        public static Boolean Save()
        {
            Boolean returnValue = false;

            try
            {
                //write to file
                switch (MVCSettings.SerializeAs)
                //switch (Settings.SerializeAs)
                {
                    // case MVCSettings.SerializationFormat.Json:
                    //     {
                    //         SettingsController.PersistJson(Settings, FilePath);

                    //         break;
                    //     }
//                    case MVCSettings.SerializationFormat.DataContract:
//                        {
//                            SettingsController.PersistDataContract(Settings, FilePath);
//
//                            break;
//                        }
                    case MVCSettings.SerializationFormat.Xml:
                    default:
                        {
                            SettingsController.PersistXml(Settings, FilePath);

                            break;
                        }
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
             
            return returnValue;
        }

        /// <summary>
        /// Loads the specified object with data from the specified file, using XML Serializer.
        /// </summary>
        /// <param name="filePath"></param>
        public static MVCSettings LoadXml(String filePath)
        {
            MVCSettings returnValue = null;
            Type returnValueType = null;

            try
            {
                //XML Serializer of type Settings
                returnValueType = typeof(MVCSettings);//returnValue.GetType();
                XmlSerializer xs = new XmlSerializer(returnValueType);

                //Stream reader for file
                StreamReader sr = new StreamReader(filePath);

                //de-serialize into Settings
                returnValue = (MVCSettings)xs.Deserialize(sr);
                returnValue.Sync();

                //close file
                sr.Close();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Saves the specified object's data to the specified file, using XML Serializer.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="filePath"></param>
        public static void PersistXml(MVCSettings settings, String filePath)
        {
            try
            {
                //XML Serializer of type Settings
                XmlSerializer xs = new XmlSerializer(settings.GetType());

                //Stream writer for file
                StreamWriter sw = new StreamWriter(filePath);

                //serialize out of Settings
                xs.Serialize(sw, settings);

                //close file
                sw.Close();

                settings.Sync();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);

                throw;
            }
        }

//        /// <summary>
//        /// Loads the specified object with data from the specified file, using DataContract Serializer.
//        /// </summary>
//        /// <param name="filePath"></param>
//        public static MVCSettings LoadDataContract(String filePath)
//        {
//            MVCSettings returnValue = null;
//            Type returnValueType = null;
//            DataContractSerializerSettings dataContractSerializerSettings = null;
//
//            try
//            {
//                //DataContract Serializer of type Settings
//                returnValueType = typeof(MVCSettings);//returnValue.GetType();
//                dataContractSerializerSettings = new DataContractSerializerSettings();
//                dataContractSerializerSettings.PreserveObjectReferences=true;
//                dataContractSerializerSettings.MaxItemsInObjectGraph = Int32.MaxValue;
//                DataContractSerializer xs = new DataContractSerializer(returnValueType, dataContractSerializerSettings);//,null, Int32.MaxValue, false, true /* preserve object refs */, null
//
//                //Stream writer for file
//                using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
//                {
//                    //de-serialize into Settings
//                    returnValue = (MVCSettings)xs.ReadObject(fs);
//                }
//
//                returnValue.Sync();
//            }
//            catch (Exception ex)
//            {
//                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
//
//                throw;
//            }
//            return returnValue;
//        }
//
//        /// <summary>
//        /// Saves the specified object's data to the specified file, using DataContract Serializer.
//        /// </summary>
//        /// <param name="settings"></param>
//        /// <param name="filePath"></param>
//        public static void PersistDataContract(MVCSettings settings, String filePath)
//        {
//            DataContractSerializerSettings dataContractSerializerSettings = null;
//            DataContractSerializer xs = null;
//
//            try
//            {
//                //DataContract Serializer of type Settings
//                dataContractSerializerSettings = new DataContractSerializerSettings();
//                dataContractSerializerSettings.PreserveObjectReferences=true;
//                dataContractSerializerSettings.MaxItemsInObjectGraph = Int32.MaxValue;
//                xs = new DataContractSerializer(settings.GetType(), dataContractSerializerSettings);//, null, Int32.MaxValue, false, true /* preserve object refs */, null
//
//                //Stream writer for file
//                using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.Write))
//                {
//                    //serialize out of Settings
//                    xs.WriteObject(fs, settings);
//                }
//
//                settings.Sync();
//            }
//            catch (Exception ex)
//            {
//                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
//
//                throw;
//            }
//        }
//
//        /// <summary>
//        /// Loads the specified object with data from the specified file, using Json Serializer.
//        /// </summary>
//        /// <param name="filePath"></param>
//        public static MVCSettings LoadJson(String filePath)
//        {
//            MVCSettings returnValue = null;
//            Type returnValueType = null;
//            JsonSerializerSettings jsonSerializerSettings = null;
//            JsonSerializer serializer = null;
//
//            try
//            {
//                jsonSerializerSettings = new JsonSerializerSettings();
//                jsonSerializerSettings.MaxDepth = null;
//
//                // deserialize JSON directly from a file
//                using (StreamReader file = File.OpenText(filePath))
//                {//TODO:use jsonSerializerSettings
//                    serializer = new JsonSerializer();
//                    returnValue = (MVCSettings)serializer.Deserialize(file, typeof(MVCSettings));
//                    //OR
//                    //MVCSettings account = JsonConvert.DeserializeObject<MVCSettings>(json);
//                }
//
//                // //Json Serializer of type Settings
//                // returnValueType = typeof(MVCSettings);//returnValue.GetType();
//                // jsonSerializerOptions = new JsonSerializerOptions();
//                // jsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve;
//                // jsonSerializerOptions.MaxDepth = Int32.MaxValue;
//                // jsonSerializerOptions.IgnoreReadOnlyProperties = true;
//
//                // //Stream writer for file
//                // using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
//                // {
//                //     //de-serialize into Settings
//                //     returnValue = /*(MVCSettings)*/JsonSerializer.Deserialize<MVCSettings>(fs, jsonSerializerOptions);
//                //     // Console.WriteLine(String.Format("After opening {0} format is {1}...", filePath, MVCSettings.SerializeAs.ToString()));
//                // }
//
//                returnValue.Sync();
//            }
//            catch (Exception ex)
//            {
//                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
//
//                throw;
//            }
//            return returnValue;
//        }
//
//        /// <summary>
//        /// Saves the specified object's data to the specified file, using Json Serializer.
//        /// </summary>
//        /// <param name="settings"></param>
//        /// <param name="filePath"></param>
//        public static void PersistJson(MVCSettings settings, String filePath)
//        {
//            JsonSerializerSettings jsonSerializerSettings = null;
//            JsonSerializer serializer = null;
//
//            try
//            {
//                jsonSerializerSettings = new JsonSerializerSettings();
//                jsonSerializerSettings.MaxDepth = null;
//
//                // serialize JSON directly to a file
//                using (StreamWriter file = File.CreateText(filePath))
//                {//TODO:use jsonSerializerSettings
//                    serializer = new JsonSerializer();
//                    serializer.Serialize(file, settings, typeof(MVCSettings));
//                    //OR
//                    //String json = JsonConvert.SerializeObject(settings, Formatting.Indented);
//                }                
//                
//                // //Json Serializer of type Settings
//                // jsonSerializerOptions = new JsonSerializerOptions();
//                // jsonSerializerOptions.WriteIndented = true;
//                // jsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve;
//                // jsonSerializerOptions.MaxDepth = Int32.MaxValue;
//                // jsonSerializerOptions.IgnoreReadOnlyProperties = true;
//
//                // //Stream writer for file
//                // using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.Write))
//                // {
//                //     //serialize out of Settings
//                //     JsonSerializer.Serialize<MVCSettings>(fs, settings, jsonSerializerOptions);
//                // }
//
//                settings.Sync();
//            }
//            catch (Exception ex)
//            {
//                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
//
//                throw;
//            }
//        }
//
        /// <summary>
        /// Display property grid dialog. Changes to properties are reflected immediately.
        /// </summary>
        public static void ShowProperties(ConsoleApplication.DVoid refreshDelegate)
        {
            try
            {
                throw new NotImplementedException("PropertiesViewer not available in GtkSharp");
//                 //PropertiesViewer pv = new PropertiesViewer(MVCSettings.AsStatic, Refresh);
//                 PropertyDialog pv = new PropertyDialog(Settings, refreshDelegate);
// #if debug
//                 //pv.Show();//dialog properties grid validation does refresh
// #else
//                 pv.ShowDialog();//dialog properties grid validation does refresh
//                 pv.Dispose();
// #endif
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);


                throw;
            }
        }
        #endregion Methods
    }
}
