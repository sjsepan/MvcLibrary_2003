# readme.md - README for MvcLibrary 0.3

## About

App-specific library of application functions and classes for C#  applications; requires Ssepan.Application, ssepan.io, ssepan.utility
MvcLibrary back-ported to Visual C# 2003 and .Net 1.1. Note: Going from later version requires giving up several features.
~ No Action type; used delegates
~ No generics; used overloads
~ No LINQ; used for and if/else
~ Different types for menu and toolbar; used ImageList for toolbar images
~ No 'default' keyword; used explicit nulls, enums, or values

### Purpose

To serve as a reference architecture and template for console and winforms apps that share a common model and settings file.
Also demonstrates my interpretation of the Model-View-Controller pattern.

### Usage notes

~N/A

### History

0.3:
~Initial release

### Fixes

### Known Issues


Steve Sepan
ssepanus@yahoo.com
9/28/2022
