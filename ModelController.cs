﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using Ssepan.Utility;
using Ssepan.Application;

namespace MvcLibrary
{
    /// <summary>
    /// Manager for the run-time model. 
    /// </summary>
    /// <typeparam name="MVCModel"></typeparam>
    public class ModelController
    {
        #region Declarations
        protected static Boolean _ValueChanging; //used by controller methods that could trigger notifications and refresh while processing
        protected static Boolean _NoUiOnThisThread = false; //use with controller operations run from non-UI thread
        #endregion Declarations

        #region Constructors
        #endregion Constructors

        #region Properties
        private static MVCModel _Model = null;
        public static MVCModel Model
        {
            get { return _Model; }
            set 
            {
                if (DefaultHandler != null)
                {
                    if (Model != null)
                    {
                        Model.PropertyChanged -= DefaultHandler;
                    }
                }

                _Model = value;

                if (DefaultHandler != null)
                {
                    if (Model != null)
                    {
                        Model.PropertyChanged += DefaultHandler;
                    }
                }
            }
        }

        private static PropertyChangedEventHandler _DefaultHandler = null;
        /// <summary>
        /// Handler to assigned to Settings on New, Open.
        /// </summary>
        public static PropertyChangedEventHandler DefaultHandler
        {
            get { return _DefaultHandler; }
            set 
            {
                if (DefaultHandler != null)
                {
                    if (Model != null)
                    {
                        Model.PropertyChanged -= DefaultHandler;
                    }
                }

                _DefaultHandler = value;

                if (DefaultHandler != null)
                {
                    if (Model != null)
                    {
                        Model.PropertyChanged += DefaultHandler;
                    }
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// New settings
        /// </summary>
        /// <returns></returns>
        public static Boolean New()
        {
            Boolean returnValue = false;
            try
            {
                //create new object
                Model = new MVCModel();//TODO:move this class into app and use specific types

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
            return returnValue;
        }
        #endregion Methods
    }
}
